var webs = function(server, board_id) {

	if (server == 0)
		server = 'z-xxapp.rhcloud.com:8000';

	var callb = {};

	function checkCallable(f) {
		return f !== undefined && typeof(f) == 'function';
	}

	function chkC(f) {
		return checkCallable(callb[f])
	}

	var board_id_q = '?board_id='+board_id;
	var socket = io.connect('http://' + server + '/' + board_id_q);
	var get_board_cb = undefined;

	socket.on('connect', function() {
		if(!chkC('connect'))
				return;
		callb['connect']();
	});

	socket.on('userJoin', function(user_id) {
		if(!chkC('userJoin'))
				return;
		callb['userJoin'](user_id);
	});

	socket.on('userLeave', function(user_id) {
		if(!chkC('userLeave'))
				return;
		callb['userLeave'](user_id);
	});

	socket.on('addStroke-', function(data) {
		if(!chkC('addStroke'))
				return;
		callb['addStroke'](data);
	});

	socket.on('addObject-', function(data) {
		if(!chkC('addObject'))
				return;
		callb['addObject'](data);
	});

	socket.on('changeObject-', function(data) {
		if(!chkC('changeObject'))
				return;
		callb['changeObject'](data);
	});

	socket.on('removeObject-', function(data) {
		if(!chkC('removeObject'))
				return;
		callb['removeObject'](data);
	});

	socket.on('clearbs-', function(data) {
		if(!chkC('clearbs'))
				return;
		callb['clearbs'](data);
	})

	socket.on('changeTheme-', function(data) {
		if(!chkC('changeTheme'))
				return;
		callb['changeTheme'](data);
	})

	socket.on('board', function(data) {
		if(get_board_cb === undefined)
			return;
		get_board_cb(data);
	});

	socket.on('objectid', function(data) {
		if(!chkC('changeObjectid'))
				return;
		callb['changeObjectid'](data);
	});

	function CallbackHandleMng() {
	};

	CallbackHandleMng.prototype.on = function(g, f) {
		callb[g] = f;
	};

	CallbackHandleMng.prototype.strokeAdded = function(data) {
		socket.emit('addStroke', data);
	};

	CallbackHandleMng.prototype.objectAdded = function(data) {
		socket.emit('addObject', data);
	};

	CallbackHandleMng.prototype.objectChanged = function(data) {
		socket.emit('changeObject', data);
	};

	CallbackHandleMng.prototype.objectRemoved = function(data) {
		socket.emit('changeObject', data);
	};

	CallbackHandleMng.prototype.getBoard = function(f) {
		get_board_cb = f;
		socket.emit('getBoard');
	};

	CallbackHandleMng.prototype.clearbs = function() {
		socket.emit('clearbs');
	}

	CallbackHandleMng.prototype.changeTheme = function(id) {
		socket.emit('changeTheme', id);
	}

	return new CallbackHandleMng();
};