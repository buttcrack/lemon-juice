function thd() {
	$.ajax({
		url: '/board/'+board_id3+'/theme',
		method: 'GET'
	}).done(function(g) {
		setthemes(g.id)
	})
}

thd()

webs.on('changeTheme', function(j) {
	setthemes(j.id)
});

function settheme(id) {
	$.ajax({
		url: '/board/'+board_id3+'/theme',
		data : {id:id},
		method : 'POST'
	})
	setthemes(id)
	webs.changeTheme({id:id});
}

function setb(b) {
	//$("body").css({'background-size' : ''});
	$("#canv").css({'background-image' : 'url('+b+')'});
}

function setthemes(id) {
	if(id == 1) {
		setb('/paper.jpg')
	}
	if(id == 2) {
		setb('/blueprint.jpg')
	}
	if(id == 3) {
		setb('/white.jpg')
	}
}