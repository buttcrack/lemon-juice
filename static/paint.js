var canv = document.getElementById('canv');
var c = canv.getContext('2d');
var prev_p = {x : null, y : null};
var pressed = false;

var pen_s = 5;
var pen_c = 'black';
var stroke = [];
var objects = [];

function stpen(g, f) {
    pen_s = g;
    $('#pen_s').text(pen_s);
    $('.bt5').removeClass('btn-warning');
    $(f).addClass('btn-warning');
};

function stcol(g, f) {
    pen_c = g;
    $('#pen_c').text(pen_c);

    $('.bt').removeClass('btn-warning');
    $(f).addClass('btn-warning');
};

function clearbt() {
    $('.bt').removeClass('btn-primary');
}

function rand(min, max) {
    return Math.floor(Math.random()*(max - min + 1) + min);
};

$(canv).on('mousedown', function(e) {
    pressed = true;
    
    var x = e.offsetX;
    var y = e.offsetY;
    prev_p.x = x;
    prev_p.y = y;
    stroke.push(x, y);
});

$(canv).on('mouseleave', function(e) {
    pressed = false;
})

$(canv).on('mouseup', function(e) {
    console.log(stroke);
    //webs.strokeAdded({pen:0, color:pen_c, width:pen_s, points:stroke});
    stroke = [];
    pressed = false;
})

webs.on('changeObject', function(k) {
    changeObject(k);
});

function changeObject(k) {
    var kr = getObjectByGlobalid(k.id);
    if(kr) {
        delete objects[kr.loc_id];
        k.loc_id = kr.loc_id;
        objects[k.loc_id] = k;

        var j = $('[id_=id'+k.loc_id+']');

        var x = k.data.pos.x;
        var y = k.data.pos.y;
        var width = k.data.width;
        var height = k.data.height;
        
        j.css({
            left : x,// - 50,
            top : y,// - 50,
            width : width,
            height : height
        });
    }
}

function clearbs() {
    c.clearRect(0, 0, canv.width, canv.height);
    webs.clearbs();
};

window.onresize = function(e) {
    canv.width = $('#conhcanv').width();
    canv.height = window.innerHeight - 124;
    webs.getBoard(draws);
};

canv.width = $('#conhcanv').width();
canv.height = window.innerHeight - 124;
webs.getBoard(draws);

function getObjectByGlobalid(id_) {
    for (var i = objects.length - 1; i >= 0; i--) {
        if(objects[i].id == id_)
            return objects[i];
    };

    return undefined;
};

function drawStroke(stroke) {
    var pen_col = stroke.color;
    var pen_siz = stroke.width;
    var prg = {x:stroke.points[0], y:stroke.points[1]};

    for(var i = 2; i < stroke.points.length; i+=2) {
        x = stroke.points[i];
        y = stroke.points[i+1];
        if(pen_col == 'eraser')
            c.globalCompositeOperation = 'destination-out';
        else
            c.globalCompositeOperation = 'source-over';
        c.beginPath();
        if(pen_col=='eraser')
            c.strokeStyle = 'black';
        else
        c.strokeStyle = pen_col;
        c.lineCap = 'round';


        c.lineWidth = pen_siz;
        c.moveTo(prg.x, prg.y);
        c.lineTo(x, y);
        c.stroke();
        
        c.closePath();

        prg.x = x;
        prg.y = y;
    }
};

function addObject(k) {

    var x = k.data.pos.x;
    var y = k.data.pos.y;
    var width = k.data.width;
    var height = k.data.height;



    objects.push(k);
    var g = objects.length-1;
    k.loc_id = g;
    var h = createMovable();
    h.attr('id_', 'id'+k.loc_id)

    h.css({
        left : x,
        top : y,
        width : width,
        height : height
    });
    
    h.css({
        opacity : 0.0
    });
    h.appendTo($('#boxc'));
    h.fadeTo(400, 1);
    return h;
};

function draws(s) {
    var j = s.strokes;
    var k = s.objects;
    console.log(j);
    for(var i = 0; i < j.length; i++) {
        drawStroke(j[i].stroke);
    }

    for(var i = 0; i < k.length; i++) {
        addObject(k[i]);
    }
};

webs.getBoard(draws);

webs.on('addStroke', function(stroke) {
    drawStroke(stroke);
});

webs.on('changeObjectid', function(j) {
    objects[j.loc_id_].id = j.id_;
});

webs.on('addObject', function(k) {
    addObject(k);
});

webs.on('clearbs', function() {
    c.clearRect(0, 0, canv.width, canv.height);
});

$(canv).on('mousemove', function(e) {
    
    if(!pressed)
        return;
    
    var x = e.offsetX;
    var y = e.offsetY;
    
    if(prev_p.x == null) {
        prev_p.x = x;
        prev_p.y = y;
    }
    if(pen_c == 'eraser')
        c.globalCompositeOperation = 'destination-out';
    else
        c.globalCompositeOperation = 'source-over';
    c.beginPath();

    if(pen_c == 'eraser')
        c.strokeStyle = 'black';
    else {
        c.strokeStyle = pen_c;
    }
    c.lineCap = 'round';


    c.lineWidth = pen_s;
    c.moveTo(prev_p.x, prev_p.y);
    c.lineTo(x, y);
    c.stroke();
    
    c.closePath();
    
    /*c.beginPath();
    
    c.lineWidth = 0;
    c.fillStyle = pen_c;
    c.arc(x, y, pen_s/2, 0, 2 * Math.PI);
    c.fill();

    c.closePath();*/
    g = [prev_p.x, prev_p.y, x, y];
    
    prev_p.x = x;
    prev_p.y = y;

    stroke.push(x, y);
    webs.strokeAdded({pen:0, color:pen_c, width:pen_s, points:g});
    
})

function getCoords(elem) {
  // (1)
  var box = elem.getBoundingClientRect();

  var body = document.body;
  var docEl = document.documentElement;

  // (2)
  var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

  // (3)
  var clientTop = docEl.clientTop || body.clientTop || 0;
  var clientLeft = docEl.clientLeft || body.clientLeft || 0;

  // (4)
  var top = box.top + scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;

  return {
    top: top,
    left: left
  };
}

function createMovable() {
    return $('<div class="box movable"> \
            <div class="boxb"></div> \
            <div class="fhandle-h fhandle-tl"></div> \
            <div class="fhandle-h fhandle-tr"></div> \
            <div class="fhandle-h fhandle-bl"></div> \
            <div class="fhandle-h fhandle-br"></div> \
            </div>');
}

$('#add_stbt').on('mousedown', function(e) {
    var ch = getCoords(document.getElementById("boxc"));
    
    var gx = e.pageX - ch.left - 50;
    var gy = e.pageY - ch.top - 50;
    var k = {data:{pos:{x:gx, y:gy}, 
                width:50, height:50}};
    var h = addObject(k);
    webs.objectAdded(k);
    
    h.trigger('mousedown');
});

var mouse_pressed = false;
var last_coords = null;
var move_object = null;
var resize_object = null;

var getINc = (function() {
    var x = 10;
    return function() {
        return x++;
    };
})();

$('#boxc').on('mousedown', '.movable', function(e) {
    mouse_pressed = true;
    move_object = this;
    last_coords = {
        x : e.pageX,
        y : e.pageY
    };
    
    $(this).css("z-index", getINc());
});

$('#boxc').on('mousedown', '.fhandle-h', function(e) {
    mouse_pressed = true;
    resize_object = this;
    last_coords = {
        x : e.pageX,
        y : e.pageY
    };
    return false;
});

$('#boxc').on('mouseup', function(e) {
    mouse_pressed = false;
    move_object = null;
    resize_object = null;
});

function move(e, x, y) {
    $(e).css({
        left: e.offsetLeft + x + 'px',
        top: e.offsetTop + y + 'px'
    });
}

function resize(e, obj) {
    var rtop = obj.top || 0;
    var rleft = obj.left || 0;
    var rright = obj.right || 0;
    var rbottom = obj.bottom || 0;
    
//    console.log('rtop : ' + rtop);
//    console.log('rleft : ' + rleft);
//    console.log('rright : ' + rright);
//    console.log('rbottom : ' + rbottom);
    
    var ef = $(e);
    ef.css({
        left : e.offsetLeft + rleft,
        top: e.offsetTop + rtop,
        width: ef.width() - rleft - rright,
        height: ef.height() - rtop - rbottom
    });
}

$('#boxc').on('mousemove', function(e) {
    if(move_object)
        if(mouse_pressed) {
            x = e.pageX - last_coords.x;
            y = e.pageY - last_coords.y;
            
            last_coords.x = e.pageX;
            last_coords.y = e.pageY;
            move(move_object, x, y);
            var id__ = +(''+$(move_object).attr('id_')).substring(2);
            var k = objects[id__];
            if(k.id === undefined)
                return;
            var o = $(move_object); var h_ = parseFloat(o.css('left')); var g_ = parseFloat(o.css('top'));
            k.data.pos.x = h_;
            k.data.pos.y = g_;
            webs.objectChanged(k);
        }
    
    if(resize_object)
        if(mouse_pressed) {
            x = e.pageX - last_coords.x;
            y = e.pageY - last_coords.y;
            
            last_coords.x = e.pageX;
            last_coords.y = e.pageY;
            var resize_objectg = $(resize_object);
            if(resize_objectg.hasClass('fhandle-tl'))
                resize(resize_object.parentElement, {top : y, left : x});
            if(resize_objectg.hasClass('fhandle-tr'))
                resize(resize_object.parentElement, {top : y, right : -x});
            if(resize_objectg.hasClass('fhandle-bl'))
                resize(resize_object.parentElement, {bottom : -y, left : x});
            if(resize_objectg.hasClass('fhandle-br'))
                resize(resize_object.parentElement, {bottom : -y, right : -x});


            var u = $(resize_object.parentElement);
            var id__ = +(''+u.attr('id_')).substring(2);
            console.log(id__);
            var k = objects[id__];
            if(k.id === undefined)
                return;
            var o = u; var h_ = parseFloat(o.css('left')); var g_ = parseFloat(o.css('top'));
            var g__ = parseFloat(o.css('width'));
            var k__ = parseFloat(o.css('height'));
            k.data.pos.x = h_;
            k.data.pos.y = g_;
            k.data.width = g__;
            k.data.height = k__;
            webs.objectChanged(k);
            
        }
});





















