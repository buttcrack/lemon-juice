function crListitem(text, id, url) {
	return $('<li class="list-group-item" id="'+id+'"><span class="glyphicon glyphicon-file"></span> '+text+'<button type="button" class="btn btn-xs btn-warning pull-right delete-bt"><span class="glyphicon glyphicon-trash"></span>Delete</button><a href="'+url+'" class="btn btn-xs btn-success pull-right marg-r"><span class="glyphicon glyphicon-share-alt"></span>Join</a></li>"');
}


function updateLi(data) {
	var h = $('#boards');
	$('#boards').html('');
	data.forEach(function(d) {
		g = crListitem(d.name, d.board_id, d.url);
		h.append(g);
	});
}

function updateBoardList() {
	$.ajax({
		url:'/boards',
		method:'GET',
	}).done(function(data) {
		updateLi(data);
	});
}

function createBoard() {
	var f = $('#board_name').val();
	$.ajax({
		url:'/boards',
		method:'POST',
		data:{board_name:f}
	}).done(function(data) {
		console.log(data);
		updateBoardList();
		$('#board_name').val('');
	});
}

function changeName() {
	var name_old = $('#change_n').attr('data-name-or');
	var name_f = $('#change_n').val();

	if(name_old != name_f) {
		if(confirm('Do you want to change your name from '+name_old+' to '+name_f+'?')) {
			$.ajax({
				url: '/changeName',
				data: {name:name_f},
				method: 'POST'
			})
		} else {
			$('#change_n').val(name_old)
		}
	}
}

function changeEmail() {
	var email_old = $('#change_g').attr('data-email-or');
	var email_f = $('#change_g').val();

	if(email_old != email_f) {
		if(confirm('Do you want to change your email from '+email_old+' to '+email_f+'?')) {
			$.ajax({
				url: '/changeEmail',
				data: {email:email_f},
				method: 'POST'
			})
		} else {
			$('#change_g').val(email_old)
		}
	}
}

function deleteBoard(board_url) {
	$.ajax({
		url: board_url,
		method:'DELETE'
	}).done(function(data) {
		updateBoardList();
	});
}

updateBoardList();

$('#create_boardbt').on('click', function() {
	if($('#board_name').val() == '')
		return;
	createBoard();
})

$('ul').on('click', '.delete-bt', function() {
	deleteBoard('/board/'+$(this).parent().attr('id'));
})