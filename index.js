var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('./webs');
var MongoClient = require('mongodb').MongoClient;
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
var db = require('./db');
var rg = require('./routes');
var sock9 = require('./sock');

var sessionx = session({secret:"fSERGW(5g0w3gaErg3jg0w3g",cookie:{maxAge:60000000},resave:false,saveUninitialized:false});
io = io(server, sessionx);
app.use(sessionx);


sock9(io);

app.set('views', './views');
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', rg);


app.use(express.static(__dirname + '/static'));
app.use(cookieParser())

app.use(function(req, res, next) {
	res.status(404);
	res.render('404f');
})

/*app.get('/login', function(req, res) {
	res.cookie('f52g', 'fwjj805jg028jg02g');
	res.send('hello');
});*/

//mongodb_url = process.env.OPENSHIFT_MONGODB_DB_URL + 'z'
mongodb_url = "";
if(process.env.OPENSHIFT_MONGODB_DB_URL) {
	mongodb_url = process.env.OPENSHIFT_MONGODB_DB_URL + 'z';
} else {
	mongodb_url = 'mongodb://localhost:27017/z'
}

console.log(mongodb_url)

MongoClient.connect(mongodb_url, function(err, h) {
	if(err)
		console.log('mongod err');

	db.setConn(h);
})

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8000
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

server.listen(server_port, server_ip_address, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('App listening at http://%s:%s', host, port);
});