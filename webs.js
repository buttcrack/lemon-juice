var io = require('socket.io');

var callb = {}
callb.connect = undefined;
callb.disconnect = undefined;
callb.addStroke = undefined;
callb.addObject = undefined;
callb.changeObject = undefined;
callb.removeObject = undefined;
callb.getBoard = undefined;
callb.clearbs = undefined;

/*stroke = {
	user: 0,
	pen: 1,
	color: 4,
	points: [
	1, 2, 3, 4
	]
}

obj = {
	user: 0,
	loc_id: 0,
	id: 0,
	data: {
		pos: {
			x : 1,
			y: 10
		},
		data:"",
		width : 50,
		height: 50
	}
}
*/

function CallbackHandleMng() {
};

CallbackHandleMng.prototype.on = function(g, f) {
	callb[g] = f;
};

function checkCallable(f) {
	return f !== undefined && typeof(f) == 'function';
}

function chkC(f) {
	return checkCallable(callb[f])
}

module.exports = function(server, xxh) {
	if(server == undefined)
		return undefined;
	io = io(server);
	io.use(function(socket, next) {
		xxh(socket.request, socket.request.res, next);
	});
	var create_object_id = (function() {
		var __object_id = 1;
		return function() {
			return __object_id++;
		}
	})();

	io.on('connect', function(socket) {
		var board_id = socket.handshake.query['board_id'];
		var user_id = 0;

		console.log('log to ' + board_id);

		function br(s, data) {
			socket.broadcast.to(board_id).emit(s, data);
			//console.log("board_id" + board_id);
			//io.sockets.in(board_id).emit(s, data);
		};

		if(!chkC('connect'))
			return;

		var f = callb['connect'](socket, board_id, socket.handshake, socket.id);
		if (f === false)
			return;
		user_id = f;

		socket.join(board_id);

		console.log('socket : ');
		console.log(socket.rooms);
		br('userJoin', user_id);

		socket.on('disconnect', function() {
			if(!chkC('connect'))
				return;
			br('userLeave');
			callb['disconnect'](board_id, user_id);
		});

		socket.on('addStroke', function(data) {
			if(!chkC('addStroke'))
				return;
			data['user'] = user_id;
			br('addStroke-', data);
			callb['addStroke'](board_id, user_id, data);
		});

		socket.on('addObject', function(data) {
			if(!chkC('addObject'))
				return;
			id__ = create_object_id();
			socket.emit('objectid', {loc_id_:data['loc_id'], id_:id__});
			data['id'] = id__;
			data['user'] = user_id;
			br('addObject-', data);
			callb['addObject'](board_id, user_id, data);
		});

		socket.on('changeObject', function(data) {
			if(!chkC('changeObject'))
				return;
			data['user'] = user_id;
			br('changeObject-', data);
			callb['changeObject'](board_id, user_id, data);
		});

		socket.on('removeObject', function(data) {
			if(!chkC('removeObject'))
				return;
			data['user'] = user_id;
			br('removeObject-', data);
			callb['removeObject'](board_id, user_id, data);
		});

		socket.on('getBoard', function() {
			if(!chkC('getBoard'))
				return;
			var f = function(data) {
				socket.emit('board', data);
			};
			callb['getBoard'](board_id, user_id, f);
		});

		socket.on('clearbs', function() {
			if(!chkC('clearbs'))
				return;
			br('clearbs-', {data:5});
			callb['clearbs'](board_id);
		});

		socket.on('changeTheme', function(idj) {
			br('changeTheme-', idj);
			if(!chkC('changeTheme'))
				return;
			callb['changeTheme'](board_id, user_id, idj);
		})
	});

	return new CallbackHandleMng();
};