var db = require('./db');

module.exports = function(h) {
var io = h;

io.on('connect', function(socket, board_id, handshake, socketid) {
	//console.log(board_id);
	//console.log(handshake);
	//console.log(socketid);
	//console.log(socket.request.session.user_id);

	db.userIsOnline(socket.request.session.user_id, board_id);

	return socket.request.session.user_id;
});

var strokes = [];
var objects = {};

io.on('addStroke', function(board_id, user_id, stroke) {
	//console.log('stroke added board : ' + board_id + ', user : ' + user_id + ', stroke : ' + stroke);
	//strokes.push(stroke);
	db.addStroke(board_id, user_id, stroke);
});

io.on('addObject', function(board_id, user_id, obj) {
	//console.log(obj);
	//objects[obj.id] = obj;
	//console.log(objects);
});

io.on('changeObject', function(board_id, user_id, obj) {
	//objects[obj.id] = obj;
});

io.on('getBoard', function(board_id, user_id, f) {
	//var g = []
	//for(var h in objects) {
	//	if(objects.hasOwnProperty(h))
	//		g.push(objects[h]);
	//}
	//console.log(g);
	//f({strokes:strokes, objects:g});
	db.getBoard(board_id, f);
});

io.on('clearbs', function(board_id) {
	db.clearbs(board_id);
});

io.on('disconnect', function(board_id, user_id) {
	console.log(board_id);
	console.log(user_id);

	db.userIsOffline(user_id, board_id)
});

}