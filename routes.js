var r = require("express").Router();
var db = require('./db');

function redirifg(s) {
	var redi = s;
	return   function (req, res, next) {
		if(req.session.user_id)
			next();
		else
			res.redirect(redi);
	}
}

function redirifj(s) {
	var redi = s;
	return   function (req, res, next) {
		if(req.session.user_id)
			res.redirect(redi);
		else
			next();
	}
}

r.post('/register', redirifj('/user'), function(req, res) {
	var u  = {login : req.body.login,
		  passw : req.body.passw,
		  email : req.body.email,
		  name : req.body.name}
	db.register(u, req, res);
});

r.get('/register', function(req, res) {
	if(req.session.user_id != undefined){
		res.redirect('/user');
		return;
	}
	res.render('register');
});

r.post('/login', redirifj('/user'), function(req, res) {
	var u  = {login : req.body.login,
		  passw : req.body.passw}
	db.login(u, req, res);
});

r.get('/login', function(req, res) {
	if(req.session.user_id != undefined){
		res.redirect('/user');
		return;
	}

	res.render('login')
});

r.get('/', function(req, res) {
	if(req.session.user_id != undefined){
		res.redirect('/user');
		return;
	}
	res.render('index');
})

r.get('/boards', function(req, res) {
	if(req.session.user_id === undefined) {
		res.status(403).end();
		return;
	}

	db.boards(req.session.user_id, res);
});

r.get('/user', function(req, res) {
	console.log('user id = ' + req.session.user_id);
	if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}
	db.getUser(req.session.user_id, function(login, name, email) {
	res.render('user', {login:login,name:name,email:email});
	})
});

r.get('/logout', function(req, res) {
	delete req.session.user_id;
	res.redirect('/login');
})

r.post('/boards', function(req, res) {
	if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}

	var a = {user_id : req.session.user_id,
	     name : req.body.board_name,
	 	 date: Date.now()};
	db.createBoard(a, res);
})

r.post('/changeEmail', function(req, res) {
		if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}
	console.log('change email ' + req.body.email)
	db.changeEmail(req.session.user_id, req.body.email)
	res.json({r:true});
})

r.post('/changeName', function(req, res) {
		if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}
	db.changeName(req.session.user_id, req.body.name)
	res.send();
})

r.delete('/board/:id', function(req, res) {
	if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}

	db.deleteBoard(req, res);
})


r.get('/board/:id', function(req, res) {
	if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}
	db.boardexists(req.params.id, function(u, h) {
		if(!u) {
			res.redirect('/user');
			return;
		}

		res.render('board', {name:h,board_id:req.params.id})
	})
})

r.get('/board/:id/online', function(req, res) {
	if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}

	//res.json({on:db.getBoardOnline(req.params.id)});
	db.getBoardOnline(req.params.id, function(data) {
		res.json({on:data})
	})
})

r.get('/board/:id/theme', function(req, res) {
	if(req.session.user_id === undefined) {
		res.redirect('/login');
		return;
	}

	db.getThemeId(req.params.id, function(data) {
		res.json(data);
	})
});

r.post('/board/:id/theme', function(req, res) {
	if(req.session.user_id === undefined)
		res.redirect('/login');

	db.setTheme(req.params.id, req.body.id);
	res.json({r:true});
})

module.exports = r;