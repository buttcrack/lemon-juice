var uu = require('node-uuid');

db = {}
db.userOnline = {}
var conn = undefined;
db.setConn = function(h) {
	conn = h;
};

function g(login, callback) {
	conn.collection('users').find({login : login}).next(function(eer, result) {
		if(result == null)
			callback(true);
		else
			callback(false);
	})
}

db.register = function (u, req, res) {
	console.log('register');

	g(u.login, function(con) {
		if(!con) {
			res.redirect('/register');
			return;
		}

	var uuid = uu.v4();
	conn.collection('users').insert({user_id : uuid, login : u.login,
	                                 passw : u.passw, email : u.email,name:u.name}, function(err, result) {
	                                 	req.session.user_id = uuid;
	                                 	console.log(req.session.user_id);
	                                 	res.redirect('/user');
	                                 	console.log('uuid = ' + uuid);
	                                 });
	});
}

db.login = function(u, req, res) {
	conn.collection('users').find({login : u.login, passw : u.passw}).next(function(eer, result) {
		if(result == null)
			res.render('login');
		else {
			req.session.user_id = result.user_id;
			res.redirect('/user');
		}
	})
}

db.boards = function (u, res) {
	conn.collection('boards').find({user_id:u},{name:1,url:1,date:1,board_id:1,_id:0}).toArray(function(eer, result) {
		res.json(result);
	})
}

db.createBoard = function (a, res) {
	console.log('a = ' + a);
	var uuid = uu.v4();
	a.board_id = uuid;
	a.theme_id = 3;
	a.url = '/board/'+a.board_id;
	conn.collection('boards').insert(a)
	res.json({r:true});
}

db.deleteBoard = function (req, res) {
	conn.collection('boards').find({user_id:req.session.user_id, board_id:req.params.id}).next(function(eer, result) {
		if(result == null){
			res.status(403).end();
			return;
		}
		conn.collection('boards').remove({user_id:req.session.user_id, board_id:req.params.id});
		res.json({r:true});
	})
}

db.boardexists = function (board_id, callback) {
	conn.collection('boards').find({board_id:board_id}).next(function(eer, result) {
		if(result == null)
			callback(false, '');
		else
			callback(true, result.name);
	})
}

db.addStroke = function(board_id, user_id, stroke) {
	conn.collection('strokes').insert({board_id:board_id, user_id:user_id, stroke:stroke});
}

db.getUser = function(user_id, callback) {
	conn.collection('users').find({user_id:user_id}, {login:1, name:1, email:1, _id:0}).next(function(eer, result) {
		callback(result.login,result.name,result.email);
	});
}

db.changeEmail = function(user_id, email) {
	conn.collection('users').update({user_id:user_id}, {$set:{email:email}})
}

db.changeName = function(user_id, name) {
	conn.collection('users').update({user_id:user_id}, {$set:{name:name}})
}

db.getBoard = function(board_id, f) {
	conn.collection('strokes').find({board_id:board_id}, {stroke:1,_id:0}).toArray(function(eer, result) {
		f({strokes:result,objects:[]});
	})
}

db.clearbs = function(board_id) {
	conn.collection('strokes').remove({board_id:board_id});
}

db.userIsOnline = function(user_id, board_id) {
	console.log('user online')
	console.log('user_id = ' + user_id)
	console.log(db.userOnline)

	if(db.userOnline[board_id] === undefined) {
		var j = {}
		j[user_id] = 1
		db.userOnline[board_id] = j
	}
	else
		if(db.userOnline[board_id][user_id] === undefined)
			db.userOnline[board_id][user_id] = 1
		else
			db.userOnline[board_id][user_id] += 1
		console.log('ONLINE')
	console.log(db.userOnline)
}

db.userIsOffline = function(user_id, board_id) {
	console.log('user OFFLINE')
	console.log('user_id = ' + user_id)
	console.log(db.userOnline)
	if(db.userOnline[board_id] === undefined)
		db.userOnline[board_id] = {}
	else
		db.userOnline[board_id][user_id] -= 1
	console.log('offlin')
	console.log(db.userOnline)
}

db.userDisconn = function(user_id, board_id) {
	if(db.userOnline[board_id] == undefined)
		return true;
	if(db.userOnline[board_id][user_id] > 0)
		return false;
	return true;
}

db.getBoardOnline = function(board_id, callback) {
	var a = db.userOnline[board_id];
	var j = []
	for(var x in a) {
		if(a.hasOwnProperty(x))
			if(a[x] != 0)
				j.push(x)
	}

	console.log(a)
	console.log('board id ' + board_id)
	console.log('j')
	console.log(j)

	conn.collection('users').find({user_id: {$in : j}}, {_id:0, login:1, name:1}).toArray(function(eer, result) {
		callback(result)
	})
}

db.getThemeId = function(board_id, callback) {
	conn.collection('boards').find({board_id:board_id}, {_id:0, theme_id:1}).next(function(eer, result) {
		if(result == null)
			callback({id:1});
		else
			callback({id:result.theme_id});
	})
}

db.setTheme = function(board_id, id) {
	conn.collection('boards').update({board_id:board_id}, {$set:{theme_id : id}})
}

module.exports = db;